#include "stdio.h"
int a[5] = {10,20,30,40,50};
void int_copy(int *ptrA,int *ptrB, int nbr){
//copy an array of items other than strings we pass
// the function the number of items to be copied as well as the address of the array	
//where nbr is the number of integers to be copied
}
int length(int *array,int array_size){
	//Since the parameter is passed by value
	// your first argument is only a pointer
	// So sizeof(ptr) is small and does not correspond to the actual array size
	return  array_size/sizeof(array[0]);
}
void print_array(int *array, int len){
	for (int i  = 0 ; i<len;i++){
		printf("%d ", array[i]);
	}
}
int main()
{
	printf("Total array size is %lu bytes\n",sizeof(a));
	printf("Number of elements in the array %d\n",length(a,(int)sizeof(a)));
	printf("First element is %d\n", a[0]);
	int array_length = length(a,(int)sizeof(a));
	printf("Array length is %d \n",array_length);
	printf("The array looks like : ");
	int copied_array [array_length];
	print_array(a,array_length);
	printf("\nInitially empty array: ");
	print_array(copied_array,array_length);
	printf("\n");
	int *ptr = &a[0];
	int *ptr2; 
	// printf("%d",a[5]);
	// This is not doable but why can I access a[5] inside the while condition
	// printf("%d",a[4]);
	// So the condition enforces that you stop when you have ptr pointing at something beyond the last element of the array (but this must include the last element)
	int n=0;
	while(*ptr != a[array_length]){
		ptr2 = ptr;
		copied_array[n]= *(ptr2++);
		printf("ptr point at: %d\n",*(ptr++) );
		printf("n: %d\n",n );
		// ptr2=ptr++; Be very careful in your use of ptr++, this is not a variable, it changes the value of ptr. This should only be done once, per iteration then afterwards, all else is just ptr. (Otherwise it will result in only 10,30,50, cuz we are double incrementing everytime)
		n++;
		// *ptr=*ptr+1; this is BAD because you are incrementing the values in the array and not redirecting what the ptr is pointing to .
	}
	printf("Newly copied array: ");
	print_array(copied_array,array_length);
	printf("\n Result from this with a cleaner solution (as in the string example): ");
	int *pA = &(a[0]);
	int *pB;
	int copied_array2[array_length];
	pB = copied_array2;
	while(*pA != a[array_length])/* while pA is not pointing at a null character (terminating character in string)*/
    {
        *pB++ = *pA++;// copy the character pointed to by pA to the space pointed to by pB then increment pA so it points to the next character and pB so it points to the next space
    }
    print_array(copied_array2,array_length);
    printf("\n");

	return 0;
}
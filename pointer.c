// #include <stdio.h>
// int main(){
// 	// Initialize a pointer variable 
//     int *ptr;
//     int k  = 9;
//     // Reference pointer to the variable k's
//     // by assigning the pointer to store the address
//     ptr = &k;
//     // Dereferencing
//     *ptr =7;
//     printf("%d\n", *ptr);
//     // Is there a way to print memory address stored in the pointer? 
//     printf("%d\n", *ptr);

// }
#include <stdio.h>
int j, k;
int *ptr;
int main(void)
{
	j = 1;
	k = 2;
	ptr = &k;
	printf("\n");
	printf("j has the value %d and is stored at %p\n", j, (void *)&j); 
	printf("k has the value %d and is stored at %p\n", k, (void *)&k); 
	printf("ptr has the value %p and is stored at %p\n", ptr, (void	*)&ptr);
	printf("The value of the integer pointed to by ptr is %d\n", *ptr);
	ptr = &j;
	printf("After Dereferencing pointer to &j\n");
	printf("ptr has the value %p and is stored at %p\n", ptr, (void	*)&ptr);
	printf("The value of the integer pointed to by ptr is %d\n", *ptr);
}
// #include <stdio.h>

// int main()
// {
	// To compile do this 
	// gcc helloworld.c -o helloworld
	// gcc helloworld.o 
    //printf("Hello world\n");
    //return 0;
    // char string[] = "Hello World yo! ";
    // printf("%s\n", string);
    // return 0;
//     int TRUE = 1 ;
//     // Seems like you have to manually define T and F as 0 or 1 or else compile time error
//     while (TRUE){
// 		printf("Hello World\n");
// 		TRUE  = +1;
// 	} 
// 	return 0;
// 	printf ("ALOHA!!");

// }

// #include<stdio.h>
// int main(){
// 	char name;
// 	printf("What is the first letter in your name: ");
// 	scanf("%s",&name);
// 	printf("%s\n",&name);

// }

// #include <stdio.h>
// #include <string.h>
 
// int main()
// {
// 	// Defining strings a , b 
//    char a[100], b[100];
 
//    printf("Enter your name\n");
//    gets(a);
 
//    printf("Enter the second string\n");
//    gets(b);

//    // How to do string assignment?
//    // b = "Doris";
 
//    if(strcmp(a,b) == 0)
//       printf("Entered strings are equal.\n");
//    else
//       printf("Entered strings are not equal.\n");
 
//    return 0;
//    // Note: 
//    // & get address of a variable
//    // * dereference operator : get value pointed to 
// }

// To execute without having to press the exe file do ./<program_name>
// ex) ./helloworld

#include <stdio.h>
#include <string.h>
char string[]= "Hello yo! ";
char string2[20];
char com[20];
double val;
double factorial (int n){
	// printf("%"i)
	val = 1;
	// remember to initialize i=1 and not just do i=0 
	// so that you are not multiplying by 0 every time to get product of 0
	for (int i =1; i<=n; i++ ) {
  		// printf("yo");
		val = val * i;
		// printf("i: %i\n", i);
		printf("val: %f\n", val);
	}
	return val;
}
double recursive_factorial(int n){
	if (n==1){
		return 1;
	}
	else{
		return n*recursive_factorial(n-1);
	}
}
int main()
{
	int test_int = 10;
	double tmp = factorial(test_int);
	double tmp2 = recursive_factorial(test_int);
	printf("%f\n", tmp);
	if(tmp ==tmp2){
		printf("Yay! Verified results are equal\n" );
	}
	// printf("Enter yo command: \n");
	// fgets(com,20,stdin);
	// printf("%s\n", com);
	// strcpy(string,com);
	// printf("%s\n",string);
	// // In C you can not directly print the string by calling on its variale, it must be called by the args. printf(string);
	// strcpy(string2, string);
	// printf("Copied yo: %s\n", string2);
}

#include <stdio.h>
char strA[80] = "A string to be used for demonstration purposes"; 
char strB[80];
char *my_strcpy(char *destination, char *source){
	char *p = destination;
	while (*source!='\0'){
		*p++= *source++;
	}
	*p = '\0';
	return destination;
}
int main(void)
{
	printf("-------------------------------------\nString excercise\n");
	// Pointers
    char *pA;
    char *pB;
    puts(strA);
    /* point pA at string A */
    pA = strA;
    /* show what pA is pointing to */
    puts(pA);
    /* point pB at string B */
    pB = strB;
    /* move down one line on the screen */
    putchar('\n');
    printf("After putchar\n");
    while(*pA != '\0')/* while pA is not pointing at a null character (terminating character in string)*/
    {
        *pB++ = *pA++;// copy the character pointed to by pA to the space pointed to by pB then increment pA so it points to the next character and pB so it points to the next space
    }
    *pB = '\0'; // Need to do this so that the string is nul terminated
    puts(strB); 
    printf("Testing my_strcpy\n");
    // char *source_pointer="Bob";
    // char *destination_pointer;
    // my_strcpy(destination_pointer,source_pointer);
    // puts(destination_pointer);
    my_strcpy(strB,strA);
    puts(strB);
    return 0;
}

#include <stdio.h>
int main(void){
	printf("-------------------------------------\nArray excercise\n");
	int some_array[] = {10,20,30,40,50};
	printf("The array is stored at %p\n",&some_array );
	int *ptr2;
	ptr2 = &some_array[0]; //Point the pointer to the 1st element of the array
	// C doesn't have a conventient way to do __str__ prints for arrays
	for (int i = 0; i < 5; i++)
	{
		//Indexing
		printf("%d , ", some_array[i] );
		// Reference Using pointer arithmetic
		// * returns the value that the pointer points to
		// Shifting to next memory address
		// printf("%d\n",*(ptr2+1) ); 
		//Perhaps a neater way to do this
		printf("%d\n",*(ptr2++) ); 
		//These should yield the same result
	}
	printf("Verify the same result is obtained if using array name as constant pointer");
	ptr2 = some_array;
	for (int i = 0; i < 5; i++)
	{
		//Indexing
		printf("%d , ", some_array[i] );
		// Reference Using pointer arithmetic
		printf("%d\n",*(ptr2++) ); 
		//These should yield the same result
	}

	return 0; 
}
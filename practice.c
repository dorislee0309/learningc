int main()
{
// int strlen(char *string);
// • compute the length of string
	return 0;
}

// • int strcmp(char *str1, char *str2);
// • return 0 if str1 and str2 are identical (how is
// this different from str1 == str2?)
// • char *strcpy(char *dst, char *src);
// • copy the contents of string src to the memory
// at dst. The caller must ensure that dst has
// enough memory to hold the data to be copied.